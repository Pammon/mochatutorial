module.exports = {};

/**
*   create()
*   ========
*
*   Creates and returns a new Bank Account Object
*   This allows us to have privately scoped variables
*/
module.exports.create = function(startAmount) {

    // A non exported variable to hold the balance that our
    var balance = 0;

    var Account = function(startAmount) {
    	if(parseInt(startAmount)) balance = startAmount;
    };

    // Returns the current balance of the account
    Account.prototype.getBalance = function() {
        return balance;
    };

    // Lodges money into the account
    Account.prototype.lodge = function(amount) {
    	if(parseInt(amount)>=0)
        	balance = parseInt(amount) + parseInt(balance);
    };

    //Withdraw money from the account
    Account.prototype.withdraw = function(amount){
    	balance = parseInt(balance) - parseInt(amount);
    }

    // Return the new instance of the Account Object
    return new Account(startAmount);
};