Repository URL = https://Pammon@bitbucket.org/Pammon/mochatutorial.git

Tutorial 1
Tutorial URL http://madhukaudantha.blogspot.jp/2013/11/mocha-javascript-test-framework-for.html

Command List:
npm install -g mocha ==> require administrator access
sudo npm install -g mocha ==> pass
mocha test.js
mocha test.js -R spec
npm install should
mocha carTest.js -R list
----------
Tutorial 2
Tutorial URL https://pooreffort.com/blog/testing-node-apps-with-mocha/

Command List:
npm install chai
mocha test ==> error: suite is not defined
mocha --ui tdd test ==> pass
make test ==> make: Nothing to be done for `test'. ==> solve by changing spacebars in Makefile to tabs
make test ==> /bin/sh: ./node_modules/.bin/mocha: No such file or directory ==> solve by changing the paths in Makefile to just 'mocha'
make test ==> pass with pending test ==> define a pending test in bank-account.test.js
make test ==> fail (object is not a function) ==> implement Account class in bank-account.js
make test ==> passing