var should = require('should');
var car = require("./car.js");
var assert = require("assert")
 
var mycar = new car({
    name : "Vitz",
    engineSize : 1000
});
 
describe('#Car()', function() {
    it('Car enginesize method test', function() {
        mycar.getEngineSize().should.equal(1000);
 
    })
})
describe('#Car()', function() {
    it('Car name test', function() {
        mycar.name.should.equal('Vitz');
 
    })
})